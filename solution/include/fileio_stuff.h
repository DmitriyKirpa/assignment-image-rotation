#ifndef FILEIO_H
#include <stdio.h>

#define FILEIO_H

enum file_status {
    FILE_OPENED = 0,
    READ_CRUSHED,
    WRITE_CRUSHED
};

enum file_status open_read_file(char* name, FILE** file);
enum file_status open_write_file(char* name, FILE** file);

#endif
