#ifndef IMG_H
#include <stdint.h>

#define IMG_H

struct __attribute__((packed)) pixel {
    uint8_t r, g, b;
};

struct image {
    struct pixel* content;
    uint64_t width;
    uint64_t height;
};

#endif
