#ifndef FORMATS_OUT_H
#include <stdio.h>

#define FORMATS_OUT_H

// write block >>>

enum write_status {
    WRITE_OK,
    WRITE_ERROR
};

enum write_status to_bmp(struct image const* source, FILE* destination_file);

#endif
