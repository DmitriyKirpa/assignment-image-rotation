#ifndef FORMATS_H
#include <malloc.h>
#include <stdint.h>

#include "image_layer.h"

#define FORMATS_H
#define BMP_SIGN 19778

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


// general r-w block >>>

enum work_type {
    INPUT,
    OUTPUT
};

int manage_pad(uint32_t width, FILE* file, enum work_type type);

#endif
