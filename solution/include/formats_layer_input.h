#ifndef FORMATS_IN_H
#include <stdio.h>

#define FORMATS_IN_H

enum read_status {
  READ_OK = 0,
  READ_INVALID_BITS,
  READ_INVALID_SIGNATURE,
  READ_INVALID_HEADER_IMG_WIDTH,
  READ_INVALID_HEADER_IMG_HEIGHT,
  READ_INVALID_HEADER_COLOURS,
  READ_FAILED_GETTING_HEADER,
  READ_MALLOC_FAILED
};

enum read_status from_bmp(struct image* destination, FILE* source_file);

#endif
