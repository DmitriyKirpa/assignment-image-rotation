#include <stdint.h>
#include <stdio.h>

#include "formats_layer_data.h"
#include "image_layer.h"

#define BMP_SIGN 19778

// general r-w block >>>

int manage_pad(uint32_t width, FILE* file, enum work_type type) {
    uint8_t repeat=(4 - width * sizeof(struct pixel)  % 4) % 4;
    uint8_t pad=0;
    for(uint8_t i=0; i<repeat; i++) {
        if(type == INPUT) {
            size_t read_elements_number = fread(&pad, sizeof(uint8_t), 1, file);
	    if(read_elements_number != 1) {
		fprintf(stderr, "Reading pad crashed. Check out formats_layer_data.c");
		return 1;
	    }
	}
        if(type == OUTPUT) {
            size_t written_elements_number = fwrite(&pad, sizeof(uint8_t), 1, file);
	    if(written_elements_number != 1) {
		fprintf(stderr, "Writing pad crashed. Check out formats_layer_data.c");
		return 1;
	    }
	}
    }
    return 0;
}
