#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "image_layer.h"
#include "fileio_stuff.h"
#include "formats_layer_input.h"
#include "formats_layer_output.h"
#include "transform_stuff.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    
    if((!argv) || (!(argv + 1)) || (!(argv + 2))) return 1;
    
    FILE* file_input = NULL;
    FILE* file_output = NULL;
    
    enum file_status file_in_result = open_read_file(argv[1], &file_input);
    if(file_in_result != FILE_OPENED) {
	char error_message[200] = "";
        if(file_in_result == READ_CRUSHED) strcpy(error_message, "Read crushed. The reason is somewhere in fileio_stuff layer");
	fprintf(stderr, "Oops, opening input file failed because of the following error:\n%s\n", error_message);
	return file_in_result;
    }
    
    enum file_status file_out_result = open_write_file(argv[2], &file_output);
    if(file_out_result != FILE_OPENED) {
	char error_message[200] = "";
        if(file_in_result == WRITE_CRUSHED) strcpy(error_message, "Read crushed. The reason is somewhere in fileio_stuff layer");
	fprintf(stderr, "Oops, opening output file failed because of the following error:\n%s\n", error_message);
	return file_out_result;
    }
    
    struct image read_image;
    enum read_status read_result = from_bmp(&read_image, file_input);
    if(read_result != READ_OK) {
	char error_message[200] = "";
	if(read_result == READ_INVALID_BITS) strcpy(error_message, "Input image is damaged");
	if(read_result == READ_INVALID_SIGNATURE) strcpy(error_message, "Read image does not have BMP format");
	if(read_result == READ_INVALID_HEADER_IMG_WIDTH) strcpy(error_message, "Width of input file is zero");
	if(read_result == READ_INVALID_HEADER_IMG_HEIGHT) strcpy(error_message, "Height of input image is zero");
	if(read_result == READ_INVALID_HEADER_COLOURS) strcpy(error_message, "The set of colours is incorrect");
	if(read_result == READ_FAILED_GETTING_HEADER) strcpy(error_message, "The header is damaged");
	if(read_result == READ_MALLOC_FAILED) strcpy(error_message, "Can not perform malloc");
	fprintf(stderr, "Oops, reading BMP from input file failed because of the following error:\n%s\n", error_message);
	return read_result;
    }
    
    struct image transformed_image = rotate(&read_image);
    if(!(transformed_image.content)) {
	fprintf(stderr, "Oops!!! Rotation failed\n");
	return 1;
    }
    
    enum write_status write_result = to_bmp(&transformed_image, file_output);
    if(write_result != WRITE_OK) {
	char error_message[200] = "";
        if(read_result == WRITE_ERROR) strcpy(error_message, "Can not write to empty pointer");
	fprintf(stderr, "Oops, writing BMP to output file failed because of the following error:\n%s\n", error_message);
	return write_result;
    }
    
    fflush(file_output);
    fclose(file_output);
    fclose(file_input);
    
    free(transformed_image.content);
    free(read_image.content);
    
    return 0;
}
