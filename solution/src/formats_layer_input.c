#include <malloc.h>

#include "formats_layer_data.h"
#include "formats_layer_input.h"
#include "image_layer.h"

// read block >>>

struct bmp_header_result {
    int is_ok;
    struct bmp_header header;
};

static struct bmp_header_result read_bmp_header(FILE* source_file) {
    struct bmp_header header;
    size_t read_elements_number = fread(&header, sizeof(struct bmp_header), 1, source_file);
    if(read_elements_number != 1) {
	struct bmp_header_result error = { .is_ok=0, .header=header };
	return error;
    }
    struct bmp_header_result success_read = { .is_ok=1, .header=header };
    return success_read;
}

struct pixel_result {
    int is_ok;
    struct pixel pixel;
};

static struct pixel_result read_pixel(FILE* source_file) {
    struct pixel current_pixel;
    size_t read_elements_number = fread(&current_pixel, sizeof(struct pixel), 1, source_file);
    if(read_elements_number != 1) {
	struct pixel_result error = { .is_ok=0, .pixel=current_pixel };
	return error;
    }
    struct pixel_result read_success = { .is_ok=1, .pixel=current_pixel };
    return read_success;
}

static enum read_status check_header(struct bmp_header header) {
    uint16_t pixel_size = header.biBitCount;
    if(header.bfType != BMP_SIGN) return READ_INVALID_SIGNATURE;
    if(pixel_size != 8 * sizeof(struct pixel)) return READ_INVALID_HEADER_COLOURS;
    return READ_OK;
}

enum read_status from_bmp(struct image* destination, FILE* source_file) {
    if(destination == NULL) return READ_INVALID_BITS;
    
    struct bmp_header_result bhr = read_bmp_header(source_file);
    if(!bhr.is_ok) return READ_FAILED_GETTING_HEADER;
    struct bmp_header header = bhr.header;
    enum read_status header_read_status = check_header(header);
    if(header_read_status != READ_OK) return header_read_status;
    uint16_t pixel_size = header.biBitCount;
    
    uint32_t width = header.biWidth, height = header.biHeight;
    if(width == 0) return READ_INVALID_HEADER_IMG_WIDTH;
    if(height == 0) return READ_INVALID_HEADER_IMG_HEIGHT;
    
    destination->content = malloc(width * height * pixel_size);
    if(!(destination->content)) return READ_MALLOC_FAILED;
    
    int pixel_number = 0;
    for(int y=0; y<height; y++) {
        for(int x=0; x<width; x++) {
            struct pixel_result read_pixel_info = read_pixel(source_file);
	    if(!read_pixel_info.is_ok) return READ_INVALID_BITS;
            *(destination->content + pixel_number) = read_pixel_info.pixel;
            pixel_number++;
        }
        int manage_pad_failed = manage_pad(width, source_file, INPUT);
	if(manage_pad_failed) return READ_INVALID_BITS;
    }
    
    destination->width = width;
    destination->height = pixel_number / width;
    return READ_OK;
}
