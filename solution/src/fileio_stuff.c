#include <stdio.h>

#include "fileio_stuff.h"

enum file_status open_read_file(char* name, FILE** file) {
    if(!name) return READ_CRUSHED;
    *file = fopen(name, "rb");
    if(!file) return READ_CRUSHED;
    return FILE_OPENED;
}

enum file_status open_write_file(char* name, FILE** file) {
    if(!name) return WRITE_CRUSHED;
    *file = fopen(name, "wb");
    if(!file) return WRITE_CRUSHED;
    return FILE_OPENED;
}
