#include "formats_layer_data.h"
#include "formats_layer_output.h"
#include "image_layer.h"

// write block >>>

static void bmp_header_generator(struct bmp_header* header, struct image const* image) {
    uint32_t width = image->width;
    uint32_t height = image->height;
    
    if(header != NULL) {
        header->bfType = BMP_SIGN;
        header->biWidth = width;
        header->biHeight = height;
        header->biBitCount = sizeof(struct pixel) * 8;
    }
}

static int write_header(struct image const* source, FILE* destination_file) {
    struct bmp_header header = { .bfType = -1 };
    bmp_header_generator(&header, source);
    size_t written_elements_number = fwrite(&header, sizeof(struct bmp_header), 1, destination_file);
    if(written_elements_number == 1) return 0;
    return 1;
}

static int write_pixel(struct pixel* written_pixel, FILE* destination_file) {
    size_t written_elements_number = fwrite(written_pixel, sizeof(struct pixel), 1, destination_file);
    if(written_elements_number == 1) return 0;
    return 1;
}

enum write_status to_bmp(struct image const* source, FILE* destination_file) {
    uint32_t pixel_number=0;
    if(source == NULL) return WRITE_ERROR;
    uint32_t width = source->width, height = source->height;
    write_header(source, destination_file);
    for(uint32_t y=0; y<height; y++) {
        for(uint32_t x=0; x<width; x++) {
            int pixel_failed = write_pixel(source->content + pixel_number, destination_file);
	    if(pixel_failed) return WRITE_ERROR;
            pixel_number++;
        }
        int pad_failed = manage_pad(width, destination_file, OUTPUT);
	if(pad_failed) return WRITE_ERROR;
    }
    return WRITE_OK;
}
