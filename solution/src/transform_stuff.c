#include <malloc.h>

#include "image_layer.h"

// rotation block >>>

static struct pixel* rotate_pixels(struct pixel* src, int width, int height) {
    struct pixel* rotated = malloc(width * height * sizeof(struct pixel));
    if(!rotated) return NULL;
    int tmp_counter = 0;
    for(int y=0; y<width; y++) {
        for(int x=height-1; x>=0; x--) {
            rotated[tmp_counter] = src[x * width + y];
            tmp_counter++;
        }
    }
    return rotated;
}

struct image rotate(struct image const* src) {
    struct image rotated;
    if(src != NULL) {
        rotated.content = rotate_pixels(src->content, src->width, src->height);
        rotated.width = src->height;
        rotated.height = src->width;
        return rotated;
    }
    rotated.content = NULL;
    rotated.width = 0; rotated.height = 0;
    return rotated;
}
